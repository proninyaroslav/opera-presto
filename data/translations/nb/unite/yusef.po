# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:13-02:00\n"
"PO-Revision-Date: 2009-10-26 21:54+0100\n"
"Last-Translator: Torstein Larsson <torstein.larsson.nb@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"

#. A link to preview an application as a visitor.
#: TO BE ADDED
msgid "Preview as visitor"
msgstr "Forhåndsvis som gjest"

#. A link to stop previewing an application as a visitor and
#. go back to 'owner mode'.
#: TO BE ADDED
msgid "Exit visitor preview"
msgstr "Avslutt forhåndsvisning som gjest"

#. A link to go to the owner's Home service and view his applications.
#: TO BE ADDED
msgid "My applications"
msgstr "Mine applikasjoner"

#. A link to go to view the owner's friends' applications.
#: TO BE ADDED
msgid "My friends' applications"
msgstr "Mine venners applikasjoner"

#. A link for visitors to go to the owner's Home service and view his applications.
#: TO BE ADDED
msgid "{username}'s applications"
msgstr "{username}s applikasjoner"

#. A link for visitors to go to view the owner's friends' applications.
#: TO BE ADDED
msgid "{username}'s friends' applications"
msgstr "{username}s venners applikasjoner"

#. A link for the owner to edit his message to his visitors.
#: TO BE ADDED
msgid "Edit this text"
msgstr "Endre denne teksten"

#. A heading for the Sharing section in an application.
#: TO BE ADDED
msgid "Sharing"
msgstr "Deler"

#. A sentence that suggests to a user to share via E-mail/Facebook/Twitter/Delicious.
#. Eg. Share via: [icon for e-mail] [icon for facebook] [icon for twitter] [icon for delicious]
#: TO BE ADDED
msgid "Share via:"
msgstr "Del via:"

#. An alternative text for a button that shares the application link via e-mail.
#: TO BE ADDED
msgid "Send via e-mail"
msgstr "Send via e-post"

#. An alternative text for buttons (alt='') that shares the application link on
#. Facebook/Twitter. Examples: 'Share on Facebook', 'Share on Twitter'.
#: TO BE ADDED
msgid "Share on "
msgstr "Del på"

#. An alternative text for a button (alt='') that saves the application link on
#. Delicious. Example: 'Save to Delicious'.
#: TO BE ADDED
msgid "Save to Delicious"
msgstr "Lagre på Delicious"

#. A heading for a section that describes what Opera Unite is.
#: TO BE ADDED
msgid "What is Opera Unite?"
msgstr "Hva er Opera Unite?"

#. A paragraph of a text that describes what Opera Unite is.
#: TO BE ADDED
msgid "Opera Unite is a new way to share, connect and collaborate directly between computers across the Web."
msgstr "Opera Unite er en ny måte å dele, ha forbindelser og samarbeide direkte mellom datamaskiner over nettet."

#. A paragraph of a text that describes what Opera Unite is.
#: TO BE ADDED
msgid "This Web page comes directly from {username}'s personal computer."
msgstr "Denne nettsiden kommer direkte fra {username}s personlige datamaskin."

#. A link for users that want to learn more about Opera Unite. It links to unite.opera.com.
#: TO BE ADDED
msgid "Learn more"
msgstr "Lær mer"

#. A heading for the privacy settings for the application.
#: libraries/yusef/plugins/acl/acl.js
msgid "Privacy options for {serviceName}"
msgstr "Personverninnstillinger for {serviceName}"

#. Label on the radio button controlling the privacy settings for an application, when a password is required
#: libraries/yusef/plugins/acl/acl.js
msgid "Password protected"
msgstr "Passordbeskyttet"

#. Description of the privacy settings for an application, when a password is required (Password protected)
#: libraries/yusef/plugins/acl/acl.js
msgid "Visitors must know the password"
msgstr "Gjester må vite passordet"

#. Label on the radio button controlling the privacy settings for an application, when no password is required
#: libraries/yusef/plugins/acl/acl.js
msgid "Public"
msgstr "Offentlig"

#. Description of the privacy settings for an application, when no password is required (Public)
#: libraries/yusef/plugins/acl/acl.js
msgid "Anyone may visit your {serviceName}"
msgstr "Alle kan besøke din {serviceName}"

#. A button that saves the values of the form for privacy settings.
#: TO BE ADDED
msgid "Save changes"
msgstr "Lagre endringer"

#. A feedback message that notifies the user to have a longer password.
#: TO BE ADDED
msgid "For your own security, please enter at least 4 letters or numbers."
msgstr "For din egen sikkerhet, vennligst skriv inn minst 4 bokstaver eller tall."

#. The footer text of every page in an Opera Unite application.
#. The complete text is 'Powered by Opera Unite', but Opera Unite
#. is not to be translated.
#: TO BE ADDED
msgid "Powered by"
msgstr "Levert av"

#. Label for the access control setting where a password is required
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Password protected"
msgstr "Passordbeskyttet"

#. Label text for the password associated with the limited access setting
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Password:"
msgstr "Passord:"

#. Button text to confirm the password entered
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Log in"
msgstr "Logg inn"

#. Message to a visitor who does not enter the correct password
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "The password was incorrect. Try again, or contact <A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM> for the password.</A>."
msgstr "Passordet var feil. Prøv igjen, eller kontakt <A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM> for å få passordet.</A>."

#. Error message about cookies
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Cookies are required for authentication. If you have enabled them, <A href=\"http://{hostName}{servicePath}/{requestSection}/{requestPath}\">please click here to reload this page and try again.</A>"
msgstr "Infokapsler er nødvendig for autentisering. Hvis du har tillatt infokapsler, <A href=\"http://{hostName}{servicePath}/{requestSection}/{requestPath}\">vennligst klikk her for å laste siden på nytt og prøve igjen.</A>"

#. Greeting portion of the message to visitors when access is set to Limited
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Dear visitor,"
msgstr "Kjære gjest,"

#. Message to visitors when access is set to Limited
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "You can only visit this application if {username} has given you the password."
msgstr "Du kan besøke denne applikasjonen bare hvis {username} har gitt deg passordet."

#. Wrapup portion of the message to visitors when access is set to Limited.
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "You can contact <A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM></A> to ask for the password."
msgstr "Du kan kontakte <A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM></A> og be om passordet."

#. Warning header when a password is short
#: libraries/yusef/plugins/acl/acl.js
msgid "Short password"
msgstr "Kort passord"

#. Warning message when a password is short
#: libraries/yusef/plugins/acl/acl.js
msgid "For your own security, please enter at least 4 letters or numbers."
msgstr "For din egen sikkerhet, vennligst skriv inn minst 4 bokstaver eller tall."

#. Warning header when the password is included in the link
#: libraries/yusef/plugins/acl/acl.js
msgid "Password attached to link"
msgstr "Passord er koblet til lenke"

#. Warning message when the password is included in the link
#: libraries/yusef/plugins/acl/acl.js
msgid "If a visitor forwards the link to a stranger, the stranger will also have access."
msgstr "Hvis en gjest videresender lenken til fremmede, vil de også få tilgang."

#. Label text for option to attach the password to the sharing link
#: TO BE ADDED
msgid "Attach password to the Sharing link"
msgstr "Koble passord til lenke for deling"

#. Description text for option to attach the password to the sharing link
#: TO BE ADDED
msgid "Lets friends have access by clicking on a single link. Use with caution."
msgstr "Gi venner tilgang ved å klikke på en enkel lenke. Brukes med varsomhet."

#. Description text for option to attach the password to the sharing link
#: TO BE ADDED
msgid "Invalid password. Allowed characters are a-z, A-Z and 0-9."
msgstr "Ugyldig passord. Tillatte tegn er a-z, A-Z og 0-9."

#. A heading to a template error page that shows when a user is trying to go to an incorrect URL.
#: TO BE ADDED
msgid "Page not found"
msgstr "Siden ble ikke funnet"

#. A sentence that comes before suggestions of what a user can do to correct the error.
#. Eg. 'Try any of the following:
#.         Option 1
#.         Option 2
#: TO BE ADDED
msgid "Try any of the following:"
msgstr "Prøv et alternativ nedenfor:"

#. A suggestion of what a user can do to correct an error.
#: TO BE ADDED
msgid "make sure the Web address is spelled correctly"
msgstr "forsikre deg om at nettadressen er rett stavet"

#. A suggestion of what a user can do to correct an error.
#: TO BE ADDED
msgid "return to <A href=\"{servicePath}/\">the start page of {username}'s {serviceName}</A>"
msgstr "gå tilbake til <A href=\"{servicePath}/\">startsiden for {username}s {serviceName}</A>"

#. Text shown to users who have either turned off Javascript, or are using a browser that does not support it. The application will work without javascript, though it will lose some of its advanced features.
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Please enable JavaScript to use all features of this application."
msgstr "Vennligst aktiver JavaScript for å bruke alle egenskaper ved denne applikasjonen."

